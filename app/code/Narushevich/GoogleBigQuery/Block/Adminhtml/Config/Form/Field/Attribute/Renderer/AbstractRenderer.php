<?php
namespace Narushevich\GoogleBigQuery\Block\Adminhtml\Config\Form\Field\Attribute\Renderer;

use Magento\Framework\Data\OptionSourceInterface as AttributeList;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

class AbstractRenderer extends Select
{
    protected $attributeLists;
    protected $removeEmptyOption;
    protected $isMultiple;

    public function __construct(
        Context $context,
        AttributeList $attributeList,
        array $data = [],
        $removeEmptyOption = false,
        $isMultiple = false
    ) {
        parent::__construct($context, $data);
        $this->attributeLists = $attributeList;
        $this->removeEmptyOption = $removeEmptyOption;
        $this->isMultiple = $isMultiple;
        $this->setIsRenderToJsTemplate(true);
    }

    public function setInputName($value)
    {
        if ($this->isMultiple) {
            $value .= '[]';
        }
        return $this->setName($value);
    }

    protected function _beforeToHtml()
    {
        if (!$this->getOptions()) {
            if (!$this->removeEmptyOption) {
                $this->addOption('', __($this->getData('empty_option_label') ?: ' '));
            }

            foreach ($this->attributeLists->toOptionArray() as $option) {
                if (!isset($option['value'])) {
                    continue;
                }
                if (is_scalar($option['value']) && !strlen($option['value'])) {
                    continue;
                }
                if (!isset($option['label'])) {
                    $option['label'] = '';
                }
                $this->addOption($option['value'], $option['label']);
            }
        }
        if ($this->isMultiple) {
            $this->setExtraParams('multiple="multiple"');
        }
        return parent::_beforeToHtml();
    }
}
