<?php

namespace Narushevich\GoogleBigQuery\Block\Adminhtml\Google;

use Magento\Backend\Block\Template;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Narushevich\GoogleBigQuery\Model\ChartsPool;

class View extends Template
{
    private ChartsPool $chartsPool;

    public function __construct(
        Template\Context $context,
        ChartsPool $chartsPool,
        array $data = [],
        ?JsonHelper $jsonHelper = null,
        ?DirectoryHelper $directoryHelper = null
    ) {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
        $this->chartsPool = $chartsPool;
    }

    public function getChartsList(): array
    {
        return $this->chartsPool->getEnabledModels();
    }
}
