<?php

namespace Narushevich\GoogleBigQuery\Model;

abstract class AbstractChartModel
{
    /**
     * Get chart type for rendering, src: https://www.chartjs.org/docs/latest/charts/
     */
    abstract public function getChartType(): string;

    /**
     * Get identifier for GBT data exchange
     */
    abstract public function getDataGBTIdentifier(): string;

    /**
     * Get Label of chart model
     */
    abstract public function getLabel(): string;

    /**
     * Get labels array for chart
     */
    abstract public function getChartLabels(): string;

    /**
     * Get data fow showing in chart
     */
    abstract public function getChartData(): string;
}
