<?php

namespace Narushevich\GoogleBigQuery\Model\ChartModels;

use Narushevich\GoogleBigQuery\Model\AbstractChartModel;

class ErrorsChartModel extends AbstractChartModel
{
    const CHART_TYPE     = 'bar';
    const GBT_IDENTIFIER = 'errors';
    const LABEL          = 'Errors';

    public function getChartType(): string
    {
        return self::CHART_TYPE;
    }

    public function getDataGBTIdentifier(): string
    {
        return self::GBT_IDENTIFIER;
    }

    public function getLabel(): string
    {
        return self::LABEL;
    }

    public function getChartData(): string
    {
        $data = [1400,1750,427,2318,745];
        return implode(',', $data);
    }

    public function getChartLabels(): string
    {
        $data = ["18.05.2022", "19.05.2022", "20.05.2022", "21.05.2022", "22.05.2022"];
        return '"' . implode('","', $data) . '"';
    }
}
