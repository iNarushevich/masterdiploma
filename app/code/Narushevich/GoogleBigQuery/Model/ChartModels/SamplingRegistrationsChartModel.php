<?php

namespace Narushevich\GoogleBigQuery\Model\ChartModels;

use Narushevich\GoogleBigQuery\Model\AbstractChartModel;

class SamplingRegistrationsChartModel extends AbstractChartModel
{
    const CHART_TYPE     = 'line';
    const GBT_IDENTIFIER = 'sampling_registrations';
    const LABEL          = 'Sampling Registrations';

    public function getChartType(): string
    {
        return self::CHART_TYPE;
    }

    public function getDataGBTIdentifier(): string
    {
        return self::GBT_IDENTIFIER;
    }

    public function getLabel(): string
    {
        return self::LABEL;
    }

    public function getChartData(): string
    {
        $data = [1423321,3234,100000,999999,153234];
        return implode(',', $data);
    }

    public function getChartLabels(): string
    {
        $data = ["18.05.2022", "19.05.2022", "20.05.2022", "21.05.2022", "22.05.2022"];
        return '"' . implode('","', $data) . '"';
    }
}
