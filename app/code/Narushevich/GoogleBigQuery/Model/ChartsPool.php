<?php

namespace Narushevich\GoogleBigQuery\Model;

use Narushevich\GoogleBigQuery\Model\Config;

class ChartsPool
{
    /**
     * @var $chartsModels AbstractChartModel[]
     */
    protected array $chartsModels;

    private Config $config;

    public function __construct(Config $config, array $chartsModels = [])
    {
        $this->config = $config;
        $this->chartsModels = $chartsModels;
    }

    public function getChartModels(): array
    {
        return $this->chartsModels;
    }

    public function getEnabledModels(): array
    {
        $enabledModels = [];
        $modelsConfig = $this->config->getChartsEnablingConfig();
        foreach ($this->chartsModels as $chartsModel) {
            if (isset($modelsConfig[$chartsModel->getDataGBTIdentifier()])
                && $modelsConfig[$chartsModel->getDataGBTIdentifier()]
            ) {
                $enabledModels[] = $chartsModel;
            }
        }
        return $enabledModels;
    }
}
