<?php

namespace Narushevich\GoogleBigQuery\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;

class Config
{
    const CHARTS_ENABLING_PATH = 'web/google_big_table/charts_enabling_settings';
    const CHARTS_COLUMN        = 'chart_list_column';
    const STATUS_COLUMN        = 'status_column';

    private ScopeConfigInterface $scopeConfig;
    private Json $json;

    public function __construct(ScopeConfigInterface $scopeConfig, Json $json)
    {
        $this->scopeConfig = $scopeConfig;
        $this->json = $json;
    }

    public function getChartsEnablingConfig()
    {
        $config = $this->json->unserialize($this->scopeConfig->getValue(self::CHARTS_ENABLING_PATH));
        if (!is_array($config)) {
            return [];
        }

        $result = [];
        foreach ($config as $data) {
            $result[$data[self::CHARTS_COLUMN]] = $data[self::STATUS_COLUMN];
        }
        return $result;
    }
}
