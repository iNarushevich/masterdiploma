<?php

namespace Narushevich\GoogleBigQuery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Narushevich\GoogleBigQuery\Model\ChartsPool;

class ChartsList implements OptionSourceInterface
{
    protected ChartsPool $chartsPool;

    public function __construct(ChartsPool $chartsPool)
    {
        $this->chartsPool = $chartsPool;
    }

    public function toOptionArray()
    {
        $chartModels = $this->chartsPool->getChartModels();
        $options = [];
        foreach ($chartModels as $chartModel) {
            $options[] = [
                'value' => $chartModel->getDataGBTIdentifier(),
                'label' => $chartModel->getLabel() . ' [' . $chartModel->getDataGBTIdentifier() . ']'
            ];
        }

        return $options;
    }
}
