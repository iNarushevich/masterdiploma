<?php

namespace Narushevich\GoogleBigQuery\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class YesNo implements OptionSourceInterface
{
    public function toOptionArray(): array
    {
        return [
            [
                'value' => '1',
                'label' => 'Yes'
            ],
            [
                'value' => '0',
                'label' => 'No'
            ]
        ];

    }
}
