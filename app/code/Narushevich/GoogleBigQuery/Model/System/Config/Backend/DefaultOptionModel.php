<?php

namespace Narushevich\GoogleBigQuery\Model\System\Config\Backend;

use Magento\Config\Model\Config\Backend\Serialized;

class DefaultOptionModel extends Serialized
{
    public function beforeSave()
    {
        $value = [];
        $attributes = (array)$this->getValue();
        if (!empty($attributes)) {
            unset($attributes['__empty']);
            foreach ($attributes as $attribute) {
                $value[$this->_prepareKey($attribute)] = $attribute;
            }
        }
        $this->setValue($value);
        return parent::beforeSave();
    }

    protected function _prepareKey(array $attributes = []): string
    {
        foreach ($attributes as &$attribute) {
            if (is_array($attribute)) {
                $attribute = $this->_prepareKey($attribute);
            }
        }
        return md5($this->getField() . '-' . implode('_', array_values($attributes)));
    }

    public function convertValueToArray($value)
    {
        $this->setValue($value);
        $this->_afterLoad();
        return $this->getValue();
    }
}
