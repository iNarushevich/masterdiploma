var config = {
    paths: {
        'chart': 'Narushevich_GoogleBigQuery/js/chart'
    },
    shim: {
        'chart': {
            deps: ['jquery']
        }
    }
};