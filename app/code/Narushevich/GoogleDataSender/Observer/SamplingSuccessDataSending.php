<?php
namespace Narushevich\GoogleDataSender\Observer;

use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ObserverInterface;
use  Magento\Framework\Stdlib\DateTime\DateTime;
use Narushevich\GoogleQueryBuilder\Model\QueryFormer;
use Narushevich\GoogleQueryBuilder\Model\Api\RequestBuilder;
use Narushevich\GoogleBigQuery\Model\ChartModels\SamplingRegistrationsChartModel;

class SamplingSuccessDataSending implements ObserverInterface
{
    private DateTime $dateTime;
    private LoggerInterface $logger;
    private QueryFormer $queryFormer;
    private RequestBuilder $requestBuilder;

    public function __construct(
        QueryFormer $queryFormer,
        RequestBuilder $requestBuilder,
        DateTime $dateTime,
        LoggerInterface $logger
    ) {
        $this->queryFormer = $queryFormer;
        $this->requestBuilder = $requestBuilder;
        $this->dateTime = $dateTime;
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer): void
    {
        try {
            $data = $observer->getRequestParams();
            $query = $this->queryFormer->createInsertQuery(
                SamplingRegistrationsChartModel::GBT_IDENTIFIER,
                $this->formatQueryData($data)
            );
    
            $this->requestBuilder->authorise()
                ->sendInsertRequest($query)
            ;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    private function formatQueryData(array $data): string
    {
        return $this->dateTime->gmtDate() . ', ' . $data['username'] . ', ' . $data['email'];
    }
}
