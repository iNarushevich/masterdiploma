<?php
namespace Narushevich\GoogleQueryBuilder\Model\Api;

use Magento\Framework\HTTP\Client\Curl;
use Narushevich\GoogleQueryBuilder\Model\Config;
use Narushevich\GoogleQueryBuilder\Model\Api\UrlBuilder;

class RequestBuilder
{
    private bool $isAuthorised = false;
    private Curl $curl;
    private Config $config;
    private UrlBuilder $urlBuilder;

    public function __construct(Curl $curl, Config $config, UrlBuilder $urlBuilder)
    {
        $this->curl = $curl;
        $this->config = $config;
        $this->urlBuilder = $urlBuilder;
    }

    public function authorise(): RequestBuilder
    {
        if ($this->isAuthorised) {
            return $this;
        }
        $this->curl->post($this->urlBuilder->buildAuthoriseUrl($this->config->getApiUrl()), []);
        $this->isAuthorised = true;
        return $this;
    }

    public function sendInsertRequest(string $queryData): void
    {
        $this->curl->post($this->urlBuilder->buildInsertUrl($this->config->getInsertOperationUrl(), $queryData), []);
    }

    public function sendDeleteRequest(string $queryData): void
    {
        $this->curl->post($this->urlBuilder->buildDeleteUrl($this->config->getDeleteOperationUrl(), $queryData), []);
    }
}
