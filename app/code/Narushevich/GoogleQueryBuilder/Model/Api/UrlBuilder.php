<?php
namespace Narushevich\GoogleQueryBuilder\Model\Api;

use Magento\Tests\NamingConvention\true\string;
use Narushevich\GoogleQueryBuilder\Model\Config;

class UrlBuilder
{
    private Config $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function buildAuthoriseUrl($url): string
    {
        return $this->addProjectIdToUrl($url);
    }

    public function buildInsertUrl(string $url, string $dataToInsert): string
    {
        return $this->addProjectIdToUrl($url) . $dataToInsert;
    }

    public function buildDeleteUrl(string $url, string $dataToDelete): string
    {
        return $this->addProjectIdToUrl($url) . $dataToDelete;
    }

    private function addProjectIdToUrl($url): string
    {
        return str_replace(
            '$GOOGLE_CLOUD_PROJECT',
            $this->config->getProjectId(),
            $url
        );
    }
}