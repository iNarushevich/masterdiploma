<?php
namespace Narushevich\GoogleQueryBuilder\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Tests\NamingConvention\true\string;

class Config
{
    const API_URL_PATH = 'web/google_big_table_api_configuration/api_url';
    const INSERT_URL   = 'web/google_big_table_api_configuration/insert_operation';
    const DELETE_URL   = 'web/google_big_table_api_configuration/delete_operation';
    const PROJECT_ID   = 'web/google_big_table_api_configuration/project_id';

    private ScopeConfigInterface $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getApiUrl(): string
    {
        return (string) $this->scopeConfig->getValue(self::API_URL_PATH);
    }

    public function getInsertOperationUrl(): string
    {
        return (string) $this->scopeConfig->getValue(self::INSERT_URL);
    }

    public function getDeleteOperationUrl(): string
    {
        return (string) $this->scopeConfig->getValue(self::DELETE_URL);
    }

    public function getProjectId(): string
    {
        return (string) $this->scopeConfig->getValue(self::PROJECT_ID);
    }
}
