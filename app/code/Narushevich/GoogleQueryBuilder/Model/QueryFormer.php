<?php
namespace Narushevich\GoogleQueryBuilder\Model;

class QueryFormer
{
    const INSERT_QUERY_TEMPLATE = 'INSERT INTO %s1 (%s2)';
    const SELECT_QUERY_TEMPLATE = 'SELECT * FROM %s1 WHERE %s2 %s3 %s4 %s5';

    public function createInsertQuery(string $tableName, string $dataToImport): string
    {
        return str_replace(
            ['%s2', '%s1'],
            [$dataToImport, $tableName],
            self::INSERT_QUERY_TEMPLATE
        );
    }

    public function createSearchQuery(
        string $tableName, 
        string $where = '', 
        string $groupBy = '',
        string $orderBy = '',
        string $limit = ''
    ) {
        return str_replace(
            ['%s2', '%s1', '%s3', '%s4', '%s5'],
            [$where, $tableName, $groupBy, $orderBy, $limit],
            self::SELECT_QUERY_TEMPLATE
        );
    }
}
